package py.una.fidabel.main;

import java.net.*;
import java.io.*;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;

public class Simulacro {

	public static void main(String[] args) throws IOException, JSONException {
		// TODO Auto-generated method stub
		int puerto_servidor = 4444;
		boolean listening = true;
		ServerSocket server = null;
		try {
            server = new ServerSocket(puerto_servidor);
        } catch (IOException e) {
        	System.err.println(e);
            System.err.println("No se puede abrir el puerto: ");
            System.err.println(puerto_servidor);
            System.exit(1);
        }
		
		System.out.printf("\nServer iniciado en el puerto: %d", puerto_servidor);
		
		while (listening){
			//Hasta que cancele el programa, no hace falta usar Hilos por que solo imprimimos a la consola 
			Socket socketCliente = null;
			try {
				socketCliente = server.accept();
			} catch (IOException e) {
				
				System.err.println("Fallo el accept().");
	            System.exit(1);
			}
			
			//Obtemos la entrada
			BufferedReader in = new BufferedReader(
                    new InputStreamReader(
                    socketCliente.getInputStream())
                   );
			
			String mensaje = in.readLine();
			
			JSONObject objetoJSON = new JSONObject(mensaje);
			
			System.out.printf("\nBanco ID: %d, Cotizacion: %d", objetoJSON.get("id"), objetoJSON.get("cotizacion"));
			
			in.close();
			socketCliente.close();
		}
		
		server.close();
	}

}
